# Instructions to launch Docker to install Jenkins locally on localhost

# Builds the image
docker build --tag=<container-name>

# Deploy the image
docker run <container-name>

# SSH you into the running instance
docker exec -it <container-name> /bin/bash

# Exiting your container
exit

# List the images
docker image ls
# List containers
docker container ls

# Clean up containers (exitted)
docker ps -a -f status=exited
# List all exited docker containers
docker rm $(docker ps -a -f status=exited -q)
# Remove images (images with containers cannot be removed until the container is removed)
docker image rm <image name from the list>

# Get command for login to AWS for docker
(aws ecr get-login --no-include-email --region us-west-2) #copy the output
# Tag the docker image for AWS
docker tag jenkins:latest 645148730455.dkr.ecr.us-west-2.amazonaws.com/jenkins:latest
# Push the docker image to AWS ECR
docker push 645148730455.dkr.ecr.us-west-2.amazonaws.com/jenkins:latest
